# README #
Small utility for IP address calculations including broadcast and network addresses as well as Cisco wildcard mask

![IpQalc images](http://dansoft.krasnokamensk.ru/data/1003/ipqalc_en.png)


### How do I get set up? ###
qmake

make

make install


### Who do I talk to? ###
email: dik@inbox.ru

www: http://dansoft.ru


### Copyright ###
Copyright 2019-2020 DanSoft <dik@inbox.ru>

This program is licensed under the terms and conditions of the GNU General Public License (GPLv3+); either version 3 of the License, or (at your option) any later version.
Please read the 'COPYING' file for more information.

