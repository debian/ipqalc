<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ipQalc</name>
    <message>
        <location filename="ipQalc.ui" line="+23"/>
        <source>IP Qalc</source>
        <translation>Qalcul IP</translation>
    </message>
    <message>
        <location line="+719"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location line="-684"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>MASK/CIDR</source>
        <translation>MASQUE/CIDR</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>calculate</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Netmask</source>
        <translation>Masque Net</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Wildcard</source>
        <translation>Remplacement</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>Prefix</source>
        <translation>Préfixe</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Broadcast</source>
        <translation></translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Min IP</source>
        <translation></translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Max IP</source>
        <translation></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Hosts</source>
        <translation>Hôtes</translation>
    </message>
    <message>
        <location line="-662"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IPQalc: Script for calculation of subnets&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qalcul IP : Scripte pour calculer les sous réseaux&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+140"/>
        <location line="+78"/>
        <location line="+99"/>
        <location line="+114"/>
        <location line="+78"/>
        <location line="+78"/>
        <location line="+57"/>
        <source>00.00.00.00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ipQalc.cpp" line="+97"/>
        <source>Invalid Input IP address</source>
        <translation>Entrée d&apos;adresse IP invalide</translation>
    </message>
    <message>
        <source>Invalid Input Netmask.</source>
        <translation type="vanished">Entrée du masque de réseau invalide.</translation>
    </message>
</context>
</TS>
