<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ipQalc</name>
    <message>
        <source>IP Qalc</source>
        <translation></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>IP</source>
        <translation></translation>
    </message>
    <message>
        <source>MASK/CIDR</source>
        <translation></translation>
    </message>
    <message>
        <source>calculate</source>
        <translation>Посчитать</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <source>Netmask</source>
        <translation>Маска</translation>
    </message>
    <message>
        <source>Wildcard</source>
        <translation>Инверсная маска</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation>Префикс</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Сеть</translation>
    </message>
    <message>
        <source>Broadcast</source>
        <translation>Бродкаст</translation>
    </message>
    <message>
        <source>Min IP</source>
        <translation>Мин. IP</translation>
    </message>
    <message>
        <source>Max IP</source>
        <translation>Макс. IP</translation>
    </message>
    <message>
        <source>Hosts</source>
        <translation>Хостов</translation>
    </message>
    <message>
        <source>Invalid Input IP address</source>
        <translation>Неправильно набран IP адрес</translation>
    </message>
    <message>
        <source>Invalid Input Netmask.</source>
        <translation type="vanished">Неправильно набрана маска сети.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IPQalc: Script for calculation of subnets&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IPQalc: Скрипт для подсчёта сетей&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>00.00.00.00</source>
        <translation></translation>
    </message>
</context>
</TS>
