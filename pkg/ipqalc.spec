Name:		ipqalc
Version:	1.5.3
Release:	1
Summary:	IP address calculator
Group:		Video
License:	GPLv3+
URL:		https://bitbucket.org/admsasha/ipqalc/
Source0:	https://bitbucket.org/admsasha/ipqalc/downloads/%{name}-%{version}.tar.gz

BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Widgets)

%description
Small utility for IP address calculations including broadcast 
and network addresses as well as Cisco wildcard mask.

%prep
%setup -q

%build
%qmake_qt5
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%files
%doc COPYING
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
