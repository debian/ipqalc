#ifndef IPADRESS_H
#define IPADRESS_H

#include <QString>

class IpAdress {
    public:
        explicit IpAdress(QString ipAdress,QString NetMask);
        ~IpAdress();

        QString GetIpAdress(int);
        QString GetNetMask(int);
        QString GetInversMask(int);
        QString GetBroadCast(int);
        QString GetNetAdress(int);
        QString GetMaskPrefix();
        QString GetMinIP(int);
        QString GetMaxIP(int);
        QString GetHosts();

        static QString Conv(QString,int,int);
	
    private:
        QString ipAdress;
        QString NetMask;

};

#endif
